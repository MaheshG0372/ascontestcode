package ascon;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxDriverLogLevel;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.apache.commons.io.FileUtils;
//import org.apache.commons.lang3.StringUtils;


public class Keyword {

    public static WebDriver driver;
    static Connection con = null;
    private static Statement stmt;
    public static String DB_URL;
    public static String DB_USER;
    public static String DB_PASSWORD;

    public static void openBrowser(String BrowerName) {
        switch (BrowerName) {
            case "chrome":
                DesiredCapabilities cap=DesiredCapabilities.chrome();
                cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver");
                driver = new ChromeDriver(cap);
                // ChromeOptions option=new ChromeOptions();
                // option.setHeadless(true);
                //   driver = new ChromeDriver(option);
                //  option.addArguments("--no-sandbox");
                //  driver = new ChromeDriver(option);
                driver.manage().window().maximize();
                break;
            case "firefox":
                System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver");
                Proxy p = new Proxy();
                p.setHttpProxy("192.168.12.11:8080");
                p.setFtpProxy("192.168.12.11:8080");
                p.setSslProxy("192.168.12.11:8080");
                p.setSocksProxy("192.168.12.11:8080");
                DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
                desiredCapabilities.setCapability("marionette", false);
                desiredCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                desiredCapabilities.setCapability(CapabilityType.PROXY, p);


                FirefoxOptions options = new FirefoxOptions(desiredCapabilities);
                options.setLogLevel(FirefoxDriverLogLevel.TRACE);

                driver = new FirefoxDriver(options);
                break;
            default:
                System.out.println("Brower option is invalid");
                break;

        }
    }
    public static void url(String url) {
        System.out.println("url is  "+url);
        driver.get(url);
        //System.out.println("url is ******"+url);
    }


    public void click_On_Button(String locatorType, String value) {
        try {
            By locator;
            locator = locatorValue(locatorType, value);
            WebElement element = driver.findElement(locator);
            element.click();
        } catch (NoSuchElementException e) {
            System.err.format("Noent Found to perform click" + e);
            Assert.fail();
        }
    }
    public static void waitTime(int miliseconds) throws InterruptedException {
        // System.out.println("Time wait started");
        //driver.manage().timeouts().implicitlyWait(180, TimeUnit.SECONDS);
        Thread.sleep(miliseconds);
        // System.out.println("Time wait completed");
    }

    private By locatorValue(String locatorType, String value) {
        By by;
        switch (locatorType) {
            case "id":
                by = By.id(value);
                break;
            case "name":
                by = By.name(value);
                break;
            case "xpath":
                by = By.xpath(value);
                break;
            case "css":
                by = By.cssSelector(value);
                break;
            case "linkText":
                by = By.linkText(value);

                break;
            case "partialLinkText":
                by = By.partialLinkText(value);
                break;
            case "className":
                by = By.className(value);
                break;
            default:
                by = null;
                break;
        }
        return by;
    }

    //Below method is for Navigate and double clicking the property page
    public List<String> propertypageNavigate() throws InterruptedException {
        Keyword.waitTime(3000);
        WebElement table = driver.findElement(By.className("w2ui-grid-records")).findElement(By.tagName("table"));
        WebElement tBodyEle = table.findElement(By.tagName("tbody"));
        List<WebElement> rows =tBodyEle.findElements(By.tagName("tr"));
       // System.out.println("Number of Rows including headings:" + rows.size());
        //System.out.println("ROW Values "+rows.get(0).getText());
        for (WebElement row: rows)
        {
            //System.out.println("ROW value :" +row.getText() +" =========== Row number " +row);
        }
        List<WebElement> columns = rows.get(2).findElements(By.tagName("td"));
        //System.out.println("Number of columns:" + columns.size());
        for (WebElement column: columns)
        {
            System.out.println("Column value :" +column.getText());
        }
        WebElement c = columns.get(3);
        System.out.println("C value " + c.getText());
        Actions a = new Actions(driver);
        Keyword.waitTime(1000);
        a.contextClick(c).build().perform();
        waitUntilPageLoads("cygnet-icon-arrow_back", true);
        WebElement panelBodyElement = driver.findElement(By.id("content"));
        List<WebElement> ulElementsList = panelBodyElement.findElements(By.xpath("//ul[@class='jstree-children']"));
        List<String> TabName = new ArrayList<String>();
        for (WebElement element : ulElementsList) {
            for (WebElement aTagElement : element.findElements(By
                    .className("jstree-leaf"))) {
                WebElement taga = aTagElement.findElement(By.tagName("a"));
                Keyword.waitTime(1000);

                String tabValue = taga.getText();
                //  System.out.println(tabValue);
                TabName.add(tabValue);
                taga.click();
                waitUntilPageLoads("cygnetPropertyTable", true);
                Keyword.waitTime(2000);
            }
        }

        return TabName;

    }


    //Below method is for compate the expected and actual title
    public void Validation(String actualTitle) {

        Assert.assertEquals(driver.getTitle(), actualTitle);


    }

    //Below method is for fethching the DB values
    public List<String> dbValidation(String queryName) throws Exception {
        FileReader reader = new FileReader("./conf/DB.Properties");
        Properties p = new Properties();
        p.load(reader);
        System.out.println("FILE PATH read completed");
        System.out.println("DB url is " + p.getProperty("dburl"));

        System.out.println("Loading DB drivers");
        String dbClass = "com.mysql.jdbc.Driver";
        Class.forName(dbClass).newInstance();
        System.out.println("DB Credentials");
        DB_URL = p.getProperty("dburl");
        DB_USER = p.getProperty("dbusername");
        DB_PASSWORD = p.getProperty("dbpassword");
        // DB_URL ="jdbc:mysql://192.168.11.132:3306/BOSEProdDB3Dec20bkp";
        // DB_USER="cygnet";
        //  DB_PASSWORD="12qwaszx";
        System.out.println(DB_URL+DB_USER+ DB_PASSWORD);

        Connection con = DriverManager.getConnection(DB_URL,DB_USER,DB_PASSWORD);
        // Connection con = DriverManager.getConnection("jdbc:mysql://192.168.11.132:3306/BOSEProdDB3Dec20bkp",DB_USER,DB_PASSWORD);
        System.out.println(con);
        stmt = con.createStatement();
        String query = p.getProperty(queryName);
        // String query="select name,escalationId,description,email from Users;";
        System.out.println("#######"+query);
        ResultSet res = stmt.executeQuery(query);

        List<String> expectedResult = new ArrayList<String>();

        int columCount = res.getMetaData().getColumnCount();
        while (res.next()) {
            for (int col = 1; col <= columCount; col++) {
                String cellValue = res.getString(col);
                if (cellValue == null) {
                    cellValue = "-";
                }
                expectedResult.add(cellValue);
            }

            System.out.println(expectedResult);

        }
        return expectedResult;
    }

    //Below method is for fetching the view date
    public static List<String> viewAttributes() {
        WebElement webtable = driver.findElement(By.id("content"));
        // System.out.println("webtable size"+ webtable.getSize());
        // System.out.println("WebTable content "+webtable.getText());

        // String contect=webtable.getText();

        List<WebElement> rows = webtable.findElements(By.tagName("tr"));

        int rows_count = rows.size();
        List<String> actualResult = new ArrayList<String>();

        for (int row = 0; row < rows_count; row++) {

            System.out.println("Number of Rows including headings:" + row);
            List<WebElement> columns = rows.get(row).findElements(By.tagName("td"));

            int columns_count = columns.size();


            for (int column = 0; column < columns_count; column++) {
                String attValue = columns.get(column).getText();
                //     System.out.println("####Values####:" +attValue);
                actualResult.add(attValue);

            }

        }

        return actualResult;
    }
    public static List<String> viewAttributesnew() {
        //WebElement table = driver.findElement(By.id("grid_NamedReports_Cable_System_cygnetTable_NamedReports_Cable_System_records"));

        WebElement table = driver.findElement(By.className("w2ui-grid-records"));

        WebElement webtable=table.findElement(By.tagName("table"));
        //	WebElement webelement=webtable.findElement(By.className("w2ui"));
        List<WebElement> rows = webtable.findElements(By.tagName("tr"));

        //     int rows_count = rows.size();
        List<String> actualResults = new ArrayList<String>();

        //  for (int row = 0;row < rows_count; row++) {

        //  System.out.println("Number of Rows including headings:"+row+":"+rows_count );
        List<WebElement> columns = rows.get(2).findElements(By.tagName("td"));

        int columns_count = columns.size();


        for (int column = 0; column < columns_count; column++) {
            String attValue = columns.get(column).getText();
            // System.out.println("####Values####:" +attValue);
            actualResults.add(attValue);

        }



        return actualResults;
    }

    public static List<String> expectedResult(String arrData[])
    {
        List<String> expectedResult = new ArrayList<>();


        for (String strTemp : arrData){

            // System.out.println(strTemp);
            expectedResult.add(strTemp);

        }

        return expectedResult;
    }


    //Below method is for compare the DB and view attributes
    public static void attriValidBetDBandView(List<String> actualResult, List<String> expectedResult) {
        boolean isEqual = expectedResult.equals(actualResult);
        System.out.println("$$$$Expected"+expectedResult);
        System.out.println("$$$$Actual"+actualResult);
        System.out.println("******Expected and Actualvalues matches " + isEqual);
        Assert.assertTrue(isEqual,"true");
    }

    //Below method is for taking snapshot
    public void takeSnap(String snapname) {
        Date date = new Date();
        long timeMilli = date.getTime();

        try {
            File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenshot, new File("./snap/" + snapname + timeMilli + ".jpg"));
        } catch (WebDriverException e) {
            System.out.println("The browser has been closed.");
        } catch (IOException e) {
            System.out.println("IOException : " + e.getMessage());
        }
    }

    public void enter_Text(String locatorType, String value, String text) {
        try {
            By locator;
            locator = locatorValue(locatorType, value);
            WebElement element = driver.findElement(locator);
            element.sendKeys(text);
            //System.out.println("locator name " + locatorType + " and input from text : " + text);
        } catch (NoSuchElementException e) {
            System.err.format("Noent Found to enter text" + text);
        }
    }
    public static void waitUntilPageLoads(String check,boolean waitForDataTableLoad) {
        ExpectedCondition<Boolean> expectation = driver -> {
            String acWrapperValue = null;

            try {
                acWrapperValue = (((JavascriptExecutor) driver).executeScript("return document.getElementById('super-ac-wrapper').style.display").toString());

            }
            catch (Exception e) {
                e.printStackTrace();
                acWrapperValue = "none";

            }

            return /* documentReadyState.equals("complete") && */(acWrapperValue.equals("none"))&& ((!waitForDataTableLoad) || waitUntilElementHasFinishedLoading(check));
        };

        try {
            WebDriverWait wait = new WebDriverWait(driver, 40, 10);
            wait.until(expectation);
        } catch (Throwable error) {
            System.err.println("Timeout waiting for Page Load Request to complete.");
            error.printStackTrace();
        }
    }

    public static boolean waitUntilElementHasFinishedLoading(String tag) {
        try {
            WebDriverWait block = new WebDriverWait(driver,100, 10);
            block.until(ExpectedConditions.visibilityOfElementLocated(By.className(tag)));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
    public void tabName() throws InterruptedException {
        Keyword.waitTime(3000);
        WebElement table = driver.findElement(By.className("w2ui-grid-records")).findElement(By.tagName("table"));
        WebElement tBodyEle = table.findElement(By.tagName("tbody"));
        int rowCount = 0;
        for (WebElement trElement : tBodyEle.findElements(By.tagName("tr"))) {
            if (trElement.getAttribute("class").contains("w2ui")) {
                if (!trElement.getAttribute("class").contains("w2ui-empty-record")) {
                    Keyword.waitTime(1000);
                    //   JavascriptExecutor executor = (JavascriptExecutor) driver;
                    //  executor.executeScript("arguments[0].click();", trElement);
                    Actions act = new Actions(driver);
                    //  act.doubleClick(trElement).perform();
                    act.contextClick(trElement).sendKeys(Keys.ARROW_DOWN).build().perform();
                    // .sendKeys(Keys.RETURN).build().perform();
                    waitUntilPageLoads("cygnet-icon-arrow_back", true);

                    ++rowCount;
                }

            }

        }

    }
  /*  public static List<String> viewCount()
    {
        String DataCount= driver.findElement(By.className("w2ui-footer-right")).getText();
        String ActualValuebef=StringUtils.substringAfter(DataCount,"of");
        //int ActualCount = Integer.parseInt(ActualValue);
        List<String> actualResults = new ArrayList<String>();
        //System.out.println("After cut"+ActualCount);
        String ActualValue=ActualValuebef.trim();
        actualResults.add(ActualValue);
        return actualResults;
//	return ActualCount;
    }*/
    public static void DBCountandViewCount(List<String> actualResult, List<String> expectedResult) {
        boolean isEqual = expectedResult.equals(actualResult);
        System.out.println("$$$$Expected"+expectedResult);
        System.out.println("$$$$Actual"+actualResult);
        System.out.println("******Expected and Actualvalues matches " + isEqual);
        Assert.assertTrue(isEqual,"true");
    }
    public void doubleClick(String locatorType, String value)
    {
        try {
            By locator;
            locator = locatorValue(locatorType, value);
            WebElement element = driver.findElement(locator);
            Actions a = new Actions(driver);
            a.doubleClick(element).perform();

        } catch (NoSuchElementException e) {
            System.err.format("Noent Found to perform click" + e);
            Assert.fail();
        }
    }
    public void pageHeaderValidation(String name)
    {
        String header=driver.findElement(By.tagName("h1")).getText();

        Assert.assertEquals(name, header);
    }
    public void pagePanelTitleValidation(String name)
    {
        String title=driver.findElement(By.className("propertyPanelTitle")).getText();
        Assert.assertEquals(name, title);

    }
    public static void closeBrowser()
    {
        driver.close();
    }



}
